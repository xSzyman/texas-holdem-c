﻿namespace Texas_Holdem.Models
{
    public class Hand
    {
        public string Name { get; }
        public string Suit { get; }
        public string FirstValue { get; }
        public string SecondValue { get; }
        public int Strength { get; }

        public Hand(string name, string suit, string firstValue, string secondValue, int strength)
        {
            Name = name;
            Suit = suit;
            FirstValue = firstValue;
            SecondValue = secondValue;
            Strength = strength;
        }
    }
}
