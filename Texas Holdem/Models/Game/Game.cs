﻿using System;
using System.Collections.Generic;
using System.Linq;
using Texas_Holdem.Models.Deck;
using Texas_Holdem.Models.Player;

namespace Texas_Holdem.Models.Game
{
    public class Game : IGame
    {
        public int Pot { get; set; }
        public int BigBlind { get; set; }
        public List<int> CommonCards { get; set; } = new List<int>();
        public List<IPlayer> Players { get; set; } = new List<IPlayer>();

        private readonly IDeck _deck;

        public Game(IDeck deck)
        {
            _deck = deck;
        }

        public void AddCommonCards(int quantity)
        {
            CommonCards.AddRange(_deck.PullCards(quantity));
        }

        private int GetPrevious(int index)
        {
            return index == 0 ? Players.Count - 1 : index - 1;
        }

        public void ChangeDealer()
        {
            for (var index = 0; index < Players.Count; index++)
            {
                IPlayer player = Players[index];
                if (!player.Dealer) continue;
                player.Dealer = false;
                Players[GetPrevious(index)].Dealer = true;
                Players[GetPrevious(GetPrevious(index))].Pay(BigBlind / 2);
                Players[GetPrevious(GetPrevious(GetPrevious(index)))].Pay(BigBlind);
                return;
            }
        }

        public void AddPlayers(params IPlayer[] players)
        {
            Players.AddRange(players);
        }

        public IPlayer GetStrongestPlayer()
        {
            return Players.OrderByDescending(x => x.GetStrongestHand().Strength).First();
        }

        public void GiveOutCards(int quantity)
        {
            Players.ForEach(x => x.AddPrivateCards(quantity));
        }

        public void ClearEveryPlayerCards()
        {
            Players.ForEach(x => x.RemovePrivateCards());
        }

        public IPlayer GetHighestBetPlayer()
        {
            return Players.OrderByDescending(x => x.Bet).First();
        }

        public IPlayer GetRandomPlayer()
        {
            return Players[new Random().Next(Players.Count)];
        }
    }
}
