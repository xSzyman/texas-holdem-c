﻿using System.Collections.Generic;
using Texas_Holdem.Models.Player;

namespace Texas_Holdem.Models.Game
{
    public interface IGame
    {
        void AddCommonCards(int quantity);
        void ChangeDealer();
        void AddPlayers(params IPlayer[] players);
        IPlayer GetStrongestPlayer();
        void GiveOutCards(int quantity);
        void ClearEveryPlayerCards();
        IPlayer GetHighestBetPlayer();
        IPlayer GetRandomPlayer();
        List<int> CommonCards { get; }
        int Pot { get; set; }
        List<IPlayer> Players { get; set; }
    }
}