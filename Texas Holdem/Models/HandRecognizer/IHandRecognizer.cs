﻿using System.Collections.Generic;

namespace Texas_Holdem.Models.HandRecognizer
{
    public interface IHandRecognizer
    {
        Hand SameValues(List<int> cards);
        Hand TwoPair(List<int> cards);
        Hand StraightAndFlush(List<int> cards);
        Hand HighCard(List<int> cards);
    }
}