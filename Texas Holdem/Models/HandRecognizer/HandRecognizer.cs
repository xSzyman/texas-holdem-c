﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Texas_Holdem.Models.HandRecognizer
{
    public class HandRecognizer : IHandRecognizer
    {
        private string GetSuitName(int code)
        {
            var codes = Enum.GetValues(typeof(Card)).Cast<Card>();
            return codes.First(x => (int)x == GetSuit(code)).ToString();
        }

        private string GetValueName(int code)
        {
            var codes = Enum.GetValues(typeof(Card)).Cast<Card>();
            return codes.First(x => (int)x == GetValue(code)).ToString();
        }

        private int GetSuit(int code)
        {
            return code % 4;
        }

        private int GetValue(int code)
        {
            return code / 4;
        }

        private List<int> GetSuits(List<int> cards)
        {
            var suits = new List<int>();
            cards.ForEach(x => suits.Add(GetSuit(x)));
            return suits;
        }

        private List<int> GetValues(List<int> cards)
        {
            var values = new List<int>();
            cards.ForEach(x => values.Add(GetValue(x)));
            return values;
        }

        public Hand SameValues(List<int> cards)
        {
            var values = GetValues(cards);
            var filtered = values.GroupBy(i => i).OrderByDescending(grp => grp.Count())
                .Select(grp => grp.Key).ToList();
            var first = filtered.ElementAt(0);
            var second = filtered.ElementAt(1);
            var firstOccurrence = values.Count(x => x == first);
            var secondtOccurrence = values.Count(x => x == second);
            string name = null;
            var strength = 0;
            if (firstOccurrence == 3 && secondtOccurrence == 2)
            {
                return new Hand("Full House", null, GetValueName(cards[values.IndexOf(first)]), GetValueName(cards[values.IndexOf(second)]), first * 4096 + 6);
            }
            switch (firstOccurrence)
            {
                case 4:
                    name = "Four of a Kind";
                    strength = first * 1024 + 7;
                    break;
                case 3:
                    name = "Three of a Kind";
                    strength = first * 64 + 3;
                    break;
                case 2:
                    name = "One Pair";
                    strength = first * 4 + 1;
                    break;
            }
            return firstOccurrence != 1
                ? new Hand(name, null, GetValueName(cards[values.IndexOf(first)]), null, strength)
                : null;
        }

        public Hand TwoPair(List<int> cards)
        {
            var values = GetValues(cards);
            var filtered = values.GroupBy(x => x).Where(g => g.Count() == 2).Select(g => g.Key)
                .OrderByDescending(x => x).ToList();
            return filtered.Count == 2
                ? new Hand("Two Pair", null,
                    GetValueName(cards[values.IndexOf(filtered[0])]), GetValueName(cards[values.IndexOf(filtered[1])]),
                    filtered[0] * 16 + 2)
                : null;
        }

        public Hand StraightAndFlush(List<int> cards)
        {
            var values = GetValues(cards).OrderByDescending(x => x).ToList();
            var suits = GetSuits(cards);
            bool flush = suits.All(x => x == suits.First()) && suits.Count >= 5;
            bool straight = values.Zip(values.Skip(1), (a, b) => (a - 1) == b).All(x => x);
            //Console.WriteLine("Straight: " + straight + " Flush: " + flush + "\n");
            if (straight && !flush)
            {
                return new Hand("Straight", null, GetValueName(values.Max() * 4), null, values.Max() * 256 + 4);
            }
            if (flush && !straight)
            {
                return new Hand("Flush", GetSuitName(cards.First()), GetValueName(values.Max() * 4), null, values.Max() * 1024 + 5);
            }
            if (straight && flush && values.Max() != 16)
            {
                return new Hand("Straight Flush", GetSuitName(cards.First()), GetValueName(values.Max() * 4), null,
                    values.Max() * 65536 + 8);
            }
            if (straight && flush && values.Max() == 16)
            {
                return new Hand("Royal Flush", GetSuitName(cards.First()), null, null, 4194313);
            }
            return null;
        }

        public Hand HighCard(List<int> cards)
        {
            var values = GetValues(cards);
            return new Hand("High Card", GetSuitName(cards[values.IndexOf(values.Max())]), GetValueName(cards[values.IndexOf(values.Max())]), null, values.Max());
        }

    }
}
