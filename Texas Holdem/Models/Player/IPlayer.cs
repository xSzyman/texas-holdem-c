﻿using System.Collections.Generic;

namespace Texas_Holdem.Models.Player
{
    public interface IPlayer
    {
        void AddPrivateCards(int quantity);
        void RemovePrivateCards();
        Hand GetStrongestHand(params List<int>[] lists);
        void Pay(int cash);
        void Fold();
        void Call();
        int Bet { get; set; }
        bool Dealer { get; set; }
        int Cash { get; set; }
        string Name { get; set; }
        List<int> PrivateCards { get; set; }
    }
}