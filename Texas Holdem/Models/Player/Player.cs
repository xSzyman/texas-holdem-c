﻿using System;
using System.Collections.Generic;
using System.Linq;
using Texas_Holdem.Models.Deck;
using Texas_Holdem.Models.Game;
using Texas_Holdem.Models.HandRecognizer;

namespace Texas_Holdem.Models.Player
{
    public class Player : IPlayer
    {
        public bool Dealer { get; set; }
        public int Cash { get; set; }
        public string Name { get; set; }
        public List<int> PrivateCards { get; set; } = new List<int>();
        public int Bet { get; set; }

        private readonly IDeck _deck;
        private readonly IHandRecognizer _handRecognizer;
        private readonly IGame _game;

        public Player(IDeck deck, IHandRecognizer handRecognizer, IGame game)
        {
            _deck = deck;
            _handRecognizer = handRecognizer;
            _game = game;
        }

        public void AddPrivateCards(int quantity)
        {
            PrivateCards.AddRange(_deck.PullCards(quantity));
        }

        public void RemovePrivateCards()
        {
            _deck.Cards.AddRange(PrivateCards);
            PrivateCards.Clear();
        }

        public Hand GetStrongestHand(params List<int>[] lists)
        {
            var possibleHands = new List<Hand>();
            var cards = new List<int>();
            foreach (var list in lists)
            {
                cards.AddRange(list);
            }
            possibleHands.Add(_handRecognizer.HighCard(cards));
            possibleHands.Add(_handRecognizer.SameValues(cards));
            possibleHands.Add(_handRecognizer.TwoPair(cards));
            possibleHands.Add(_handRecognizer.StraightAndFlush(cards));
            return possibleHands.Where(x => x != null).OrderByDescending(x => x.Strength).First();
        }

        public void Pay(int cash)
        {
            _game.Pot += cash;
            Bet += cash;
            Cash -= cash;
        }

        public void Fold()
        {
            //TODO
        }

        public void Call()
        {
            //TODO
        }
    }
}
