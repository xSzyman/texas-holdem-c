﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Texas_Holdem.Models.Deck
{
    public interface IDeck
    {
        void Shuffle();
        List<int> PullCards(int quantity);
        List<int> Cards { get; set; }
    }
}
