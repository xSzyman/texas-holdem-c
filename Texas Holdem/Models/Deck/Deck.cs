﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Texas_Holdem.Models.Deck
{
    public class Deck : IDeck
    {
        public List<int> Cards { get; set; }

        public Deck()
        {
            Cards = new List<int>();
            for (int suit = 0; suit < 4; suit++)
            {
                for (int value = 4; value < 17; value++)
                {
                    Cards.Add(GetCard(suit, value));
                }
            }
            Shuffle();
        }

        public void Shuffle()
        {
            var random = new Random();
            Cards = Cards.OrderBy(x => random.Next()).ToList();
        }

        private int GetCard(int suit, int value)
        {
            return suit + value * 4;
        }

        public List<int> PullCards(int quantity)
        {
            var cards = new List<int>();
            for (int i = 0; i < quantity; i++)
            {
                cards.Add(this.Cards.First());
                this.Cards.RemoveAt(0);
            }
            return cards;
        }

    }
}
