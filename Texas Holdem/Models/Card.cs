﻿namespace Texas_Holdem.Models
{
    enum Card
    {
        Club = 0,
        Diamond = 1,
        Heart = 2,
        Spade = 3,
        Two = 4,
        Three = 5,
        Four = 6,
        Five = 7,
        Six = 8,
        Seven = 9,
        Eight = 10,
        Nine = 11,
        Ten = 12,
        J = 13,
        Q = 14,
        K = 15,
        A = 16
    }
}
